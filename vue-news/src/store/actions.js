import {
  fetchNewsList,
  fetchAskList,
  fetchJobsList,
  fetchUserInfo,
  fetchItemInfo
} from '../api/index.js';

export default {
  FETCH_NEWS ({ commit }) {
    fetchNewsList()
      .then(({ data }) => {
        commit('SET_NEWS', data);
      })
      .catch(e => {
        console.error(e);
      });
  },
  FETCH_ASK ({ commit }) {
    fetchAskList()
      .then(({ data }) => {
        commit('SET_ASK', data);
      })
      .catch(e => {
        console.error(e);
      })
  },
  FETCH_ASK_DETAIL ({ commit }, id) {
    fetchUserInfo(id)
      .then(({ data }) => {
        commit('SET_ASK', data);
      })
      .catch(e => {
        console.error(e);
      })
  },
  FETCH_JOBS ({ commit }) {
    fetchJobsList()
      .then(({ data }) => {
        commit('SET_JOBS', data);
      })
      .catch(e => {
        console.error(e);
      })
  },
  FETCH_USER ({ commit }, userName) {
    fetchUserInfo(userName)
      .then(({ data }) => {
        commit('SET_USER', data);
      })
      .catch(e => {
        console.error(e);
      })
  },
  FETCH_ITEM ({ commit }, id) {
    fetchItemInfo(id)
      .then(({ data }) => {
        commit('SET_ITEM', data);
      })
      .catch(e => {
        console.error(e);
      })
  }
}
