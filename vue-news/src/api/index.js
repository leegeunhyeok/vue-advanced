import axios from 'axios';

const config = {
  baseUrl: 'https://api.hnpwa.com',
  version: 'v0'
};

function fetchNewsList () {
  return axios.get(`${config.baseUrl}/${config.version}/news/1.json`);
}

function fetchAskList () {
  return axios.get(`${config.baseUrl}/${config.version}/ask/1.json`);
}

function fetchJobsList () {
  return axios.get(`${config.baseUrl}/${config.version}/jobs/1.json`);
}

function fetchUserInfo (userName) {
  return axios.get(`${config.baseUrl}/${config.version}/user/${userName}.json`);
}

function fetchItemInfo (id) {
  return axios.get(`${config.baseUrl}/${config.version}/item/${id}.json`);
}

export {
  fetchNewsList,
  fetchAskList,
  fetchJobsList,
  fetchUserInfo,
  fetchItemInfo
}